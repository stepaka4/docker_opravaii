import os
import mysql.connector
import json
import pandas as pd
from datetime import datetime


def load_configuration(filepath=None):
    filepath = filepath or os.path.dirname(__file__) + "/configs/configurations.json"
    with open(filepath, "r") as f:
        config = json.load(f)
    return config


def db_con(db_config):
    connection = mysql.connector.connect(
        host=db_config["host"],
        database=db_config["db_name"],
        user=db_config["user"],
        password=db_config["password"],
        port=db_config["port"],
    )
    if connection.is_connected():
        print("Succesfully connected to database")
        return connection
    else:
        print("Cannot connect do database")


def insert_to_db(connector, incubator_number, temperature):
    sql = prepare_sql_data(incubator_number, temperature)
    cursor = connector.cursor()
    cursor.execute(sql)
    connector.commit()


def prepare_sql_data(incubator_number, temperature):
    now = datetime.now()
    date_time = now.strftime("%m-%d-%Y %H:%M:%S")
    sql = (
        "INSERT INTO inkubator (incubator_number, temperature, date) VALUES ("
        + incubator_number
        + ","
        + temperature
        + ",STR_TO_DATE('"
        + date_time
        + "', '%m-%d-%Y %H:%i:%s'))"
    )
    return sql


def insert_data(incubator_number, temperature):
    config = load_configuration()
    connector = db_con(config["database"])
    insert_to_db(connector, incubator_number, temperature)
    connector.close()
    return "Data has been added successfully"
