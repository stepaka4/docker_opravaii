import json
from flask import Flask
from flask_restful import Resource, Api
import receiver
import sender


app = Flask(__name__)
api = Api(app)


class insert_data(Resource):
    def get(self, incubator_number, temperature):
        return sender.insert_data(incubator_number, temperature)


class get_average(Resource):
    def get(self, incubator_number, date):
        return receiver.get_average(incubator_number, date)


class Main(Resource):
    def get(self):
        return "ABOUT APPLICATION This application is used to monitor the temperature in incubators in the neonatal ward in hospitals. The  INSERT_DATA function is used to insert data into the database in the format: incubator number/temperature/time in mm/dd/yy format (it is inserted automatically). The GET_AVERAGE function is used to obtain the daily average of temperatures for individual incubators. GRAFANA can be used to display a curve for individual daily averages. Please FOR INSERT DATA into the database use format: http://localhost:5000/insert_data/(incubator_number)/(temperature). To GET DAILY AVERAGE is need to enter the day in format: yy-MM-dd, so for get daily average use format: http://localhost:5000/get_average/ "


api.add_resource(insert_data, "/insert_data/<incubator_number>/<temperature>")
api.add_resource(get_average, "/get_average/<incubator_number>/<date>")
api.add_resource(Main, "/")

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
